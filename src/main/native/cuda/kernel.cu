extern "C"
__global__ void render_mandelbrot(const int width, const int height,const double re_offset,const double im_offset,const double re_step,const double im_step, int *output)
{


    int idX = blockIdx.x * blockDim.x + threadIdx.x;
    int idY = blockIdx.y * blockDim.y + threadIdx.y;

    if (idX >= width || idY >= height)
        return;

        int deepness = 0;
        double constRe = re_offset + (double)idX * re_step,
                constIm = im_offset +(double) idY * im_step,
                re = 0, im = 0;

        double re_square = re * re,
                im_square = im * im;

        do {
            double new_re = re_square - im_square + constRe;
            double new_im = 2 * im * re + constIm;

            re = new_re;
            im = new_im;

            re_square = re * re;
            im_square = im * im;

            deepness++;
        } while (deepness < 256 && re_square + im_square < 6.0);

        int index = idY * width + idX;
        output[index] = deepness;

}


extern "C"
__global__ void render_julia(int width, int height, double x0, double y0, double dx, double dy, int *pixels)
{
	int tX = blockIdx.x * blockDim.x + threadIdx.x;
	int tY = blockIdx.y * blockDim.y + threadIdx.y;

	if (tX >= width || tY >= height)
		return;

	int iteration = 0;
	double cRe = 0.285f,
		cIm = 0.01f,
		zRe = x0 + dx * (double)tX, Re = 0,
		zIm = y0 + dy * (double)tY, Im = 0;
	while (iteration < 256 && (zRe * zRe + zIm * zIm < 4))
	{
		Re = zRe * zRe - zIm * zIm + cRe;
		Im = 2 * zRe * zIm + cIm;
		zRe = Re;
		zIm = Im;

		++iteration;
	}

	int i = (tY * width + tX);
	pixels[i] = iteration;
}

