package demo.parallel.MandelbrotTasks;

import demo.parallel.CUDARenderer;
import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

import java.util.stream.IntStream;

public class CUDAMandelbrotSetTask extends MandelbrotSetTask {

    private static CUDARenderer cuRenderer = new CUDARenderer();

    /**
     * Creates a task to render a MandelBrot set into an image using given
     * PixelWriter with given dimensions of the image, given real and imaginary
     * values range and given rectangular area to skip. Also there is a switch
     * that disables more computational-extensive antialiasing mode.
     *
     * @param pixelWriter target to write pixels to
     * @param width       width of the image area
     * @param height      height of the image area
     * @param minR        min real value of the area
     * @param minI        min imaginary value of the area
     * @param maxR        max real value of the area
     * @param maxI        max imaginary value of the area
     * @param minX        min x value of the rectangular area to skip
     * @param minY        min y value of the rectangular area to skip
     * @param maxX        max x value of the rectangular area to skip
     * @param maxY        max y value of the rectangular area to skip
     * @param fast        fast mode disables antialiasing
     */
    public CUDAMandelbrotSetTask(PixelWriter pixelWriter, int width, int height, double minR, double minI, double maxR, double maxI, double minX, double minY, double maxX, double maxY, boolean fast) {
        super(pixelWriter, width, height, minR, minI, maxR, maxI, minX, minY, maxX, maxY, fast);
    }

    @Override
    protected void computeTask() {
        int depth_map[] = cuRenderer.render(this);

        IntStream yStream = IntStream.range(0, height).parallel();
        yStream.forEach((int y) -> {

            // We do pixels in horizontal lines always sequentially
            for (int x = 0; x < width; x++) {

                // Skip excluded rectangular area
                if (!(x >= maxX || x < minX || y >= maxY || y < minY)) {
                    continue;
                }
                Color c = getColor(depth_map[y * width + x]);

                if (isCancelled()) {
                    return;
                }
                synchronized (pixelWriter) {
                    pixelWriter.setColor(x, y, c);
                }
                hasUpdates = true;
            }
            updateProgress(progress.incrementAndGet(), height);
        });
    }
}

