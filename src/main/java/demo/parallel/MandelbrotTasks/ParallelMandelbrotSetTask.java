package demo.parallel.MandelbrotTasks;


import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

import java.util.stream.IntStream;

public class ParallelMandelbrotSetTask extends MandelbrotSetTask {

    public ParallelMandelbrotSetTask(PixelWriter pixelWriter, int width, int height, double minR, double minI, double maxR, double maxI, double minX, double minY, double maxX, double maxY, boolean fast) {
        super(pixelWriter, width, height, minR, minI, maxR, maxI, minX, minY, maxX, maxY, fast);
    }

    @Override
    protected void computeTask() {
        IntStream yStream = IntStream.range(0, height).parallel();
        yStream.forEach((int y) -> {

            // We do pixels in horizontal lines always sequentially
            for (int x = 0; x < width; x++) {

                // Skip excluded rectangular area
                if (!(x >= maxX || x < minX || y >= maxY || y < minY)) {
                    continue;
                }
                Color c;
                if (antialiased) {
                    c = calcAntialiasedPixel(x, y);
                } else {
                    c = calcPixel(x, y);
                }
                if (isCancelled()) {
                    return;
                }
                synchronized (pixelWriter) {
                    pixelWriter.setColor(x, y, c);
                }
                hasUpdates = true;
            }
            updateProgress(progress.incrementAndGet(), height);
        });
    }
}
