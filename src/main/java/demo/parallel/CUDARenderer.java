package demo.parallel;

import static jcuda.driver.JCudaDriver.*;

import demo.parallel.MandelbrotTasks.MandelbrotSetTask;
import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;

public class CUDARenderer {

    private static final int BLOCK_SIZE = 32;
    private static final String PTX_CODE_PATH = "src/main/resources/bin/common_kernels.ptx";

    private CUfunction function;
    private CUcontext context;

    public CUDARenderer() {
        create_context();
    }


    private void create_context() {

        JCudaDriver.setExceptionsEnabled(true);

        cuInit(0);
        CUdevice device = new CUdevice();
        cuDeviceGet(device, 0);

        context = new CUcontext();
        cuCtxCreate(context, 0, device);

        CUmodule module = new CUmodule();
        cuModuleLoad(module, PTX_CODE_PATH);

        function = new CUfunction();
        cuModuleGetFunction(function, module, "render_julia");

    }

    public int[] render(MandelbrotSetTask task) {
        int width = task.getWidth(),
                height = task.getHeight();

        int image_size = width * height;
        int output_image[] = new int[image_size];

        double minR = task.getMinR(),
                maxR = task.getMaxR(),
                minI = task.getMinI(),
                maxI = task.getMaxI();

        double re_step = (maxR - minR) / (double) width,
                im_step = (maxI - minI) / (double) height;


        cuCtxSetCurrent(context);
        CUdeviceptr deviceOutput = new CUdeviceptr();
        cuMemAlloc(deviceOutput, image_size * Sizeof.INT);

        Pointer kernelParameters = Pointer.to(
                Pointer.to(new int[]{width}),
                Pointer.to(new int[]{height}),
                Pointer.to(new double[]{minR}),
                Pointer.to(new double[]{minI}),
                Pointer.to(new double[]{re_step}),
                Pointer.to(new double[]{im_step}),
                Pointer.to(deviceOutput)
        );

        int gridSizeX = (width + BLOCK_SIZE - 1) / BLOCK_SIZE,
                gridkSizeY = (height + BLOCK_SIZE - 1) / BLOCK_SIZE;

        cuLaunchKernel(function,
                gridSizeX,  gridkSizeY, 1,
                BLOCK_SIZE, BLOCK_SIZE, 1,
                0, null,
                kernelParameters, null
        );
        cuCtxSynchronize();

        cuMemcpyDtoH(Pointer.to(output_image), deviceOutput,
                image_size * Sizeof.INT);

        cuMemFree(deviceOutput);

        return output_image;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        cuCtxDestroy(context);
    }
}
